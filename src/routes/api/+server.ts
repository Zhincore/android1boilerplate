import type { RequestHandler } from "./$types";
import Archiver from "archiver";
import { Readable } from "node:stream";

const boilerplateFiles = import.meta.glob("$data/**/*", {
  query: "?raw",
  import: "default",
});

type Replacements = "APPNAME" | "APPPACKAGE" | "APPSLUG" | "APPPATH" | "DATA" | "DATAITEM";

const keyWrap = (key: string, path = false) => new RegExp(`(\{%${key}%\})` + (path ? `|({{${key}}})` : ""), "g");

const slug = (str: string) =>
  str
    .toLocaleLowerCase()
    .replace(/\W+/g, "_")
    .replace(/_+/g, "_")
    .replace(/(^_)|(_$)/g, "");

function replace(str: string, replacements: Record<Replacements, string>, path = false): string {
  for (const [key, replacement] of Object.entries(replacements)) {
    str = str.replace(keyWrap(key, path), replacement);
    str = str.replace(keyWrap(key + "_slug", path), slug(replacement));
    str = str.replace(keyWrap(key + "_lower", path), replacement.toLocaleLowerCase());
  }

  return str;
}

function capitalize(str: string) {
  return str[0].toUpperCase() + str.slice(1);
}

export const POST = (async ({ request }) => {
  const data = await request.formData();
  const replacements: Record<Replacements, string> = {
    APPNAME: capitalize(data.get("APPNAME")!.toString()),
    APPPACKAGE: data.get("APPPACKAGE")!.toString(),
    APPSLUG: capitalize(data.get("APPPACKAGE")!.toString().split(".").slice(-1)[0]),
    APPPATH: data.get("APPPACKAGE")!.toString().split(".").join("/"),
    DATA: capitalize(data.get("DATA")!.toString()),
    DATAITEM: capitalize(data.get("DATAITEM")!.toString()),
  };

  const archive = Archiver("zip");
  const promises: Promise<void>[] = [];

  for (const [path, load] of Object.entries(boilerplateFiles)) {
    promises.push(
      load().then((data) => {
        const file = data as string;
        archive.append(replace(file, replacements), {
          name: replace(path.replace(/^\/data/, ""), replacements, true),
        });
      }),
    );
  }

  await Promise.all(promises);

  archive.finalize();
  return new Response(Readable.toWeb(archive) as ReadableStream, {
    headers: {
      "Content-Type": "application/zip",
      "Content-Disposition": 'attachment; filename="boilerplate.zip"',
    },
  });
}) satisfies RequestHandler;
