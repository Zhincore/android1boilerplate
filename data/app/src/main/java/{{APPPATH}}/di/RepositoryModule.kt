package {%APPPACKAGE%}.di

import {%APPPACKAGE%}.database.ILocal{%DATA%}Repository
import {%APPPACKAGE%}.database.Local{%DATA%}RepositoryImpl
import {%APPPACKAGE%}.database.{%DATA%}Dao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Provides
    @Singleton
    fun provideLocal{%DATA%}Repository(dao: {%DATA%}Dao): ILocal{%DATA%}Repository {
        return Local{%DATA%}RepositoryImpl(dao)
    }

}
