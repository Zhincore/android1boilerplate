package {%APPPACKAGE%}.di

import android.content.Context
import {%APPPACKAGE%}.database.{%DATA%}Dao
import {%APPPACKAGE%}.database.{%DATA%}Database
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): {%DATA%}Database {
        return {%DATA%}Database.getDatabase(context)
    }
}
