package {%APPPACKAGE%}.di

import {%APPPACKAGE%}.database.{%DATA%}Dao
import {%APPPACKAGE%}.database.{%DATA%}Database
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DaoModule {
    @Provides
    @Singleton
    fun provide{%DATA%}Dao(database: {%DATA%}Database): {%DATA%}Dao {
        return database.{%DATA_lower%}Dao()
    }
}
