package {%APPPACKAGE%}.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import {%APPPACKAGE%}.model.{%DATAITEM%}

@Database(entities = [{%DATAITEM%}::class], version = 1, exportSchema = true)
abstract class {%DATA%}Database : RoomDatabase() {

    abstract fun {%DATA_lower%}Dao(): {%DATA%}Dao

    companion object {
        private var INSTANCE: {%DATA%}Database? = null

        fun getDatabase(context: Context): {%DATA%}Database {
            if (INSTANCE == null) {
                synchronized({%DATA%}Database::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            {%DATA%}Database::class.java, "{%DATA_lower%}_database"
                        ).fallbackToDestructiveMigration().build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}
