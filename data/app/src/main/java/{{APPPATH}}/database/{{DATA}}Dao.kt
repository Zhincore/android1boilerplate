package {%APPPACKAGE%}.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import {%APPPACKAGE%}.model.{%DATAITEM%}
import kotlinx.coroutines.flow.Flow

@Dao
interface {%DATA%}Dao {

    @Query("SELECT * FROM {%DATA_lower%}")
    fun getAll(): Flow<List<{%DATAITEM%}>>

    @Insert
    suspend fun insert({%DATAITEM_lower%}: {%DATAITEM%}): Long
}
