package {%APPPACKAGE%}.database

import {%APPPACKAGE%}.model.{%DATAITEM%}
import kotlinx.coroutines.flow.Flow

interface ILocal{%DATA%}Repository {
    fun getAll(): Flow<List<{%DATAITEM%}>>
    suspend fun insert({%DATAITEM_lower%}: {%DATAITEM%}): Long
}
