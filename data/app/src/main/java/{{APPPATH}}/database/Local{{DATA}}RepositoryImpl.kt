package {%APPPACKAGE%}.database

import {%APPPACKAGE%}.model.{%DATAITEM%}
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class Local{%DATA%}RepositoryImpl @Inject constructor(private val dao: {%DATA%}Dao) : ILocal{%DATA%}Repository {

    override fun getAll(): Flow<List<{%DATAITEM%}>> {
        return dao.getAll()
    }

    override suspend fun insert({%DATAITEM_lower%}: {%DATAITEM%}): Long {
        return dao.insert({%DATAITEM_lower%})
    }
}
