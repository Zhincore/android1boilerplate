package {%APPPACKAGE%}

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

// Musí být v manifestu

@HiltAndroidApp
class {%APPSLUG%}Application : Application() {
  
}
