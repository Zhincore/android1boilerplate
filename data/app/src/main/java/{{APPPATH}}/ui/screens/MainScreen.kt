package {%APPPACKAGE%}.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import {%APPPACKAGE%}.navigation.INavigationRouter

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen(navigationRouter: INavigationRouter){

    val viewModel = hiltViewModel<MainScreenViewModel>()

    var fastest{%DATAITEM%}: String = "No {%DATAITEM_lower%} yet"

    viewModel.ListScreenUIState.value.let {
        fastest{%DATAITEM%} = it
    }

    Scaffold(
        topBar = {
            TopAppBar(title = {
                Text(text = stringResource(id = R.string.app_name))
            })
        },
    ) {
        Column(modifier = Modifier.padding(it)) {
            Text(text = fastest{%DATAITEM%})

            Button(onClick = {
                navigation.navigateToList()
            }) {
                Text(text = "List of {%DATA_lower%}")
            }
        }
    }
}
