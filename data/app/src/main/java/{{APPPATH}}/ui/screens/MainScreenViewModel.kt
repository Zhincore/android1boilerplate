package {%APPPACKAGE%}.ui.screens

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import {%APPPACKAGE%}.database.I{%DATA%}LocalRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainScreenViewModel @Inject constructor(
    private val repository: I{%DATA%}LocalRepository
) : ViewModel() {

    val mainScreenUIState: MutableState<String> = mutableStateOf("")

    init {
        viewModelScope.launch {
            repository.getAll().collect {
                if (it.size > 0) {
                    val sorted{%DATA%} = it.sortedBy { it.time }
                    mainScreenUIState.value = sorted{%DATA%}[0].name
                } else {
                    mainScreenUIState.value = "No {%DATAITEM_lower%} yet"
                }
            }
        }
    }



}
