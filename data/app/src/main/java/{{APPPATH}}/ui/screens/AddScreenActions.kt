package {%APPPACKAGE%}.ui.screens

interface AddScreenActions {
    fun save{%DATAITEM%}()
    fun onNameChanged(text: String)
    fun onTimeChanged(text: String)
}
