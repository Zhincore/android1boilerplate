package {%APPPACKAGE%}.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import {%APPPACKAGE%}.database.{%DATAITEM%}
import {%APPPACKAGE%}.navigation.INavigationRouter

@Composable
fun ListScreen(navigationRouter: INavigationRouter){

    val viewModel = hiltViewModel<ListScreenViewModel>()

    var {%DATA_lower%} = remember {
        mutableStateOf<List<{%DATAITEM%}>>(mutableListOf())
    }

    viewModel.listScreenUIState.value.let {
        when (it){
            is ListScreenUIState.Default -> {
                viewModel.load{%DATA%}()
            }
            is ListScreenUIState.Success -> {
                {%DATA_lower%}.value = it.{%DATA_lower%}
            }
        }
    }


    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                Text(text = "List")
            },
                navigationIcon = {
                    IconButton(onClick = {
                        navigationRouter.returnBack()
                    }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "")
                    }
                }
            )
        },
    ) {
        LazyColumn(modifier = Modifier.padding(it)) {
            {%DATA_lower%}.value.forEach {
                item {
                    Column {
                        Text(text = it.name)
                        Text(text = it.time.toString())
                    }
                }
            }
        }
    }
}
