package {%APPPACKAGE%}.ui.screens

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import {%APPPACKAGE%}.database.I{%DATA%}LocalRepository
import {%APPPACKAGE%}.database.{%DATAITEM%}
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddScreenViewModel @Inject constructor(
    private val repository: I{%DATA%}LocalRepository
) : ViewModel(), AddScreenActions {


    val addScreenUIState: MutableState<AddScreenUIState> = mutableStateOf(AddScreenUIState.Default)

    val {%DATAITEM_lower%}: {%DATAITEM%} = {%DATAITEM%}("")

    override fun save{%DATAITEM%}() {
        viewModelScope.launch {
            repository.insert({%DATAITEM_lower%})
            addScreenUIState.value = AddScreenUIState.{%DATAITEM%}Saved
        }
    }

    override fun onNameChanged(text: String) {
        {%DATAITEM_lower%}.name = text
        addScreenUIState.value = AddScreenUIState.{%DATAITEM%}StateChanged()
    }

    override fun onTimeChanged(text: String) {
        if (text.isNotEmpty()) {
            {%DATAITEM_lower%}.time = text.toInt()
        } else {
            {%DATAITEM_lower%}.time = null
        }
        addScreenUIState.value = AddScreenUIState.{%DATAITEM%}StateChanged()
    }


}
