package {%APPPACKAGE%}.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import {%APPPACKAGE%}.database.{%DATAITEM%}
import {%APPPACKAGE%}.navigation.INavigationRouter

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddScreen(navigationRouter: INavigationRouter){

    val viewModel = hiltViewModel<AddScreenViewModel>()

    var {%DATAITEM_lower%}: {%DATAITEM%} by rememberSaveable { mutableStateOf({%DATAITEM%}("")) }

    viewModel.add{%DATAITEM%}UIState.value.let {
        when(it){
            Add{%DATAITEM%}UiState.Default -> {}
            Add{%DATAITEM%}UiState.{%DATAITEM%}Saved -> {
                LaunchedEffect(it) {
                    navigation.returnBack()
                }
            }
            is Add{%DATAITEM%}UiState.{%DATAITEM%}StateChanged -> {
                {%DATAITEM_lower%} = viewModel.{%DATAITEM_lower%}
            }
        }
    }
    Scaffold(
        topBar = {
            TopAppBar(title = {
                Text(text = stringResource(id = R.string.app_name))
            })
        },
    ) {
        AddScreenContent(paddingValues = it, {%DATAITEM_lower%} = {%DATAITEM_lower%}, actions = viewModel)
    }

}

@Composable
fun AddScreenContent(
    paddingValues: PaddingValues,
    {%DATAITEM_lower%}: {%DATAITEM%},
    actions: Add{%DATAITEM%}Actions
){
    Column(modifier = Modifier.padding(paddingValues)) {

        OutlinedTextField(
            value = {%DATAITEM_lower%}.name,
            onValueChange = {
                actions.onNameChanged(it)
            },
            label = {
                Text("Name")
            }
        )

        OutlinedTextField(
            value = if ({%DATAITEM_lower%}.time != null) {%DATAITEM_lower%}.time.toString() else "",
            onValueChange = {
                actions.onTimeChanged(it)
            },
            label = {
                Text("Time")
            }
        )

        Button(onClick = { actions.save{%DATAITEM%}() }) {
            Text(text = "Save {%DATAITEM_lower%}")

        }

    }

}
