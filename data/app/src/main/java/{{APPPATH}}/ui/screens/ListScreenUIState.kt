package {%APPPACKAGE%}.ui.screens

import {%APPPACKAGE%}.database.{%DATAITEM%}

sealed class ListScreenUIState {
    class Default : ListScreenUIState()
    class Success(val {%DATA_lower%}: List<{%DATAITEM%}>) : ListScreenUIState()
}
