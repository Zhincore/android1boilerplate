package {%APPPACKAGE%}.ui.screens

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import {%APPPACKAGE%}.database.I{%DATA%}LocalRepository
import {%APPPACKAGE%}.database.{%DATAITEM%}
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListScreenViewModel @Inject constructor(
    private val repository: I{%DATA%}LocalRepository
) : ViewModel() {


    val listScreenUIState: MutableState<ListScreenUIState> = mutableStateOf(ListScreenUIState.Default())

    fun load{%DATA%}(){
        viewModelScope.launch {
            repository.getAll().collect {
                listScreenUIState.value = ListScreenUIState.Success(it)
            }
        }
    }



}
