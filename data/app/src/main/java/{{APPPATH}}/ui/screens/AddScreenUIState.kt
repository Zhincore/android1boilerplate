package {%APPPACKAGE%}.ui.screens

sealed class AddScreenUIState {
    object Default : AddScreenUIState()
    object {%DATAITEM%}Saved : AddScreenUIState()
    class {%DATAITEM%}StateChanged() : AddScreenUIState()
}
