package {%APPPACKAGE%}.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "{%DATA_lower%}")
data class {%DATAITEM%}(var name: String){

    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
}
