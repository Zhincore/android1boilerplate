package {%APPPACKAGE%}.navigation

import androidx.navigation.NavController

class NavigationRouterImpl(private val navController: NavController) : INavigationRouter {
    override fun navigateToList() {
        navController.navigate(Destination.ListScreen.route)
    }

    override fun navigateToAdd() {
        navController.navigate(Destination.AddScreen.route)
    }

    override fun returnBack() {
        navController.popBackStack()
    }

    override fun getNavController(): NavController {
        return  navController
    }
}
