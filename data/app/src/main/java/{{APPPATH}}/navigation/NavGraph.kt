package {%APPPACKAGE%}.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import {%APPPACKAGE%}.ui.screens.ListScreen

@Composable
fun NavGraph(
    navHostController: NavHostController = rememberNavController(),
    navigationRouter: INavigationRouter = remember {
        NavigationRouterImpl(navHostController)
    },
    startDestination: String
){
    NavHost(navController = navHostController, startDestination = startDestination) {
        composable(Destination.MainScreen.route){
            MainScreen(navigationRouter = navigationRouter)
        }

        composable(Destination.ListScreen.route){
            ListScreen(navigationRouter = navigationRouter)
        }

        composable(Destination.AddScreen.route){
            AddScreen(navigationRouter = navigationRouter)
        }
    }
    
}
