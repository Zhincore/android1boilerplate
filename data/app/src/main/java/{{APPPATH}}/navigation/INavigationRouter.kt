package {%APPPACKAGE%}.navigation

import androidx.navigation.NavController

interface INavigationRouter {
    fun navigateToList()
    fun navigateToAdd()
    fun returnBack()
    fun getNavController(): NavController
}
