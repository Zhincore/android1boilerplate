package {%APPPACKAGE%}.navigation

sealed class Destination(val route: String){
    object MainScreen : Destination("main")
    object ListScreen : Destination("list")
    object AddScreen : Destination("add")
}
